#!/usr/bin/env python3

from aws_cdk import core
from python_cdk.api_gateway_stack import ApiGatewayStack
from python_cdk.eks_cluster_stack import EKSClusterStack


app = core.App()
ApiGatewayStack(app, "api-gateway")
EKSClusterStack(app, "eks-cluster")

app.synth()
