import os
from aws_cdk import core
import aws_cdk.aws_apigateway as apigateway

project_name = os.environ['PROJECT_NAME']


class ApiGatewayStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, tags={'Project': project_name}, **kwargs)

        rest_api = apigateway.RestApi(
            self,
            "Word-Count-Rest-API",
            description="Word Count REST API",
            endpoint_types=[apigateway.EndpointType.REGIONAL],
            rest_api_name="Word-Count-Rest-API"
        )

        ui_resource = rest_api.root.add_resource("ui")
        ui_resource.add_method(http_method='GET')
        ui_resource.add_method(http_method='POST')
