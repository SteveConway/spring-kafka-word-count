import os

from aws_cdk import core

import yaml
from aws_cdk.aws_ec2 import InstanceType
from aws_cdk.aws_eks import Cluster
from aws_cdk.aws_iam import Role, ManagedPolicy

cloud_watch_agent_policy_name = "CloudWatchAgentServerPolicy"

region = os.environ['AWS_REGION']
project_name = os.environ['PROJECT_NAME']
cluster_name = os.environ['EKS_CLUSTER_NAME']
kubernetes_version = os.environ['EKS_VERSION']
node_group_name = os.environ['EKS_NODEGROUP_NAME']
node_type = os.environ['EKS_NODE_TYPE']
masters_role_arn = os.environ['EKS_MASTERS_ROLE_ARN']


class EKSClusterStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id,
                         env=core.Environment(region=region),
                         tags={'Project': project_name},
                         **kwargs)

        masters_role = Role.from_role_arn(
            scope=self,
            id='masters_role',
            role_arn=masters_role_arn
        )

        cluster = Cluster(
            scope=self,
            id=cluster_name,
            cluster_name=cluster_name,
            version=kubernetes_version,
            default_capacity=0,
            masters_role=masters_role
        )

        node_group = cluster.add_capacity(
            id=node_group_name,
            instance_type=InstanceType(node_type),
            desired_capacity=3,
            min_capacity=1,
            max_capacity=4
        )

        cloud_watch_agent_policy = ManagedPolicy.from_aws_managed_policy_name(
            managed_policy_name=cloud_watch_agent_policy_name
        )
        node_group.role.add_managed_policy(cloud_watch_agent_policy)

        fluentd_manifest = list(yaml.load_all(open("manifests/fluentd.yml"), Loader=yaml.FullLoader))

        cluster.add_resource(
            'CloudwatchEKSLogging',
            {
                'apiVersion': 'v1',
                'kind': 'Namespace',
                'metadata': {
                    'name': 'amazon-cloudwatch',
                    'labels': {
                        'name': 'amazon-cloudwatch'
                    }
                }
            },
            {
                'apiVersion': 'v1',
                'kind': 'ConfigMap',
                'metadata': {
                    'name': 'cluster-info',
                    'namespace': 'amazon-cloudwatch'
                },
                'data': {
                    'cluster.name': cluster_name,
                    'logs.region': region
                }
            },
            *fluentd_manifest
        )
