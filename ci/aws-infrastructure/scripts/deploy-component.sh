#!/bin/sh

COMPONENT=$1
CONTAINER_PORT=$2
SVC_PORT=$3
ECR_REPO=$4
TAG=$5

if [ "$TAG" = "" ]
then
    echo "Usage: $0 <component-name> <container port> <service port> <ECR repo URL> <tag>"
    exit 1
fi

IMAGE="${ECR_REPO}\\/${COMPONENT}:${TAG}"

cp ci/aws-infrastructure/kubernetes/deployment.yml "${COMPONENT}-deployment.yml"
sed -i "s/<COMPONENT>/${COMPONENT}/g" "${COMPONENT}-deployment.yml"
sed -i "s/<CONTAINER_PORT>/${CONTAINER_PORT}/g" "${COMPONENT}-deployment.yml"
sed -i "s/<SVC_PORT>/${SVC_PORT}/g" "${COMPONENT}-deployment.yml"
sed -i "s|<IMAGE>|${IMAGE}|g" "${COMPONENT}-deployment.yml"
sed -i "s|<DATE>|$(date)|g" "${COMPONENT}-deployment.yml"
sed -i "s|<KAFKA_USERNAME>|${KAFKA_USERNAME}|g" "${COMPONENT}-deployment.yml"
sed -i "s|<KAFKA_PASSWORD>|${KAFKA_PASSWORD}|g" "${COMPONENT}-deployment.yml"
sed -i "s|<KAFKA_BROKERS>|${KAFKA_BROKERS}|g" "${COMPONENT}-deployment.yml"
sed -i "s|<KAFKA_ZOOKEEPERS>|${KAFKA_ZOOKEEPERS}|g" "${COMPONENT}-deployment.yml"

bin/kubectl apply -f "${COMPONENT}-deployment.yml"
