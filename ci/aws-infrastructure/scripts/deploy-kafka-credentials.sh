#!/bin/sh

cp ci/aws-infrastructure/kubernetes/kafka-credentials.yml .

sed -i "s|<KAFKA_USERNAME>|${KAFKA_USERNAME}|g" kafka-credentials.yml
sed -i "s|<KAFKA_PASSWORD>|${KAFKA_PASSWORD}|g" kafka-credentials.yml

bin/kubectl apply -f kafka-credentials.yml

rm kafka-credentials.yml
