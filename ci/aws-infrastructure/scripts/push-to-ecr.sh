#!/bin/sh

COMPONENT=$1
REGION=$2
ECR_REPO=$3
TAG=$4

if [ "$TAG" = "" ]
then
    echo "Usage: $0 <component-name> <region> <ECR repo URL> <tag>"
    exit 1
fi

cd ${COMPONENT}

echo "Creating ECR repository ${COMPONENT} (will fail if it already exists)"
aws ecr create-repository --region ${REGION} --repository-name ${COMPONENT}

# Build and push docker image
docker build -t ${COMPONENT}:${TAG} .
docker tag ${COMPONENT}:${TAG} ${ECR_REPO}/${COMPONENT}:${TAG}
docker push ${ECR_REPO}/${COMPONENT}:${TAG}
