#!/bin/sh

CLUSTER_NAME=$1
REGION=$2

if [ "$REGION" = "" ]
then
    echo "Usage: $0 <cluster-name> <region>"
    exit 1
fi

mkdir bin
curl -o bin/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.7/2019-06-11/bin/linux/amd64/aws-iam-authenticator
curl -o bin/kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.7/2019-06-11/bin/linux/amd64/kubectl
chmod +x bin/aws-iam-authenticator bin/kubectl
export PATH=./bin:$PATH
echo "aws-iam-authenticator version = $(aws-iam-authenticator version)"
aws eks --region "${REGION}" update-kubeconfig --name "${CLUSTER_NAME}" --role-arn "${EKS_MASTERS_ROLE_ARN}"
echo "kubectl version = $(bin/kubectl version)"
