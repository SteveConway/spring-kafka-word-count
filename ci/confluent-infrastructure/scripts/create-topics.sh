#!/usr/bin/env bash

## Create the user topics
ccloud kafka topic create textInput --partitions 1
ccloud kafka topic create wordCount --partitions 1
ccloud kafka topic create windowedWordCount --partitions 1
