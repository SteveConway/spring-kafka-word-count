#!/usr/bin/env bash

## Delete the user topics
ccloud kafka topic delete textInput
ccloud kafka topic delete wordCount
ccloud kafka topic delete windowedWordCount

## Delete the internal topics created for streams
ccloud kafka topic list | while read -r line ; do
  if [[ $line == WordCountProcessor-* ]] || [[ $line == WindowedWordCountProcessor-* ]] ;
  then
    ccloud kafka topic delete "$line"
  fi
done
