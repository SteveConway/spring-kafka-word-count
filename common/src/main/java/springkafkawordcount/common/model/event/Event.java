package springkafkawordcount.common.model.event;

public interface Event<T> {

    T getPayload();
}
