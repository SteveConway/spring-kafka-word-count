package springkafkawordcount.textinputmonitor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import springkafkawordcount.common.model.event.TextEvent;

@Slf4j
public class TextInputMonitor {

    @StreamListener("text-input")
    public void process(TextEvent text) {
        try {
            log.info("Received text input: " + text.getPayload());
        } catch (Throwable e) {
            log.error("Failed to process text input", e);
        }
    }
}
