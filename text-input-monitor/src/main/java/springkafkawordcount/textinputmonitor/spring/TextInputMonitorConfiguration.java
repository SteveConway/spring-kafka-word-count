package springkafkawordcount.textinputmonitor.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springkafkawordcount.textinputmonitor.TextInputMonitor;

@Configuration
public class TextInputMonitorConfiguration {

    @Bean
    public TextInputMonitor textEventMonitor() {
        return new TextInputMonitor();
    }
}
