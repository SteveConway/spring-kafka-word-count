package springkafkawordcount.textinputservice.spring;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface TextInputServiceBinding {

    @Output("text-input")
    MessageChannel textInput();
}
