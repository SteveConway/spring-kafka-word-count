package springkafkawordcount.wordcountmonitor;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import springkafkawordcount.common.model.WordCount;

@Slf4j
public class WordCountMonitor {

    @StreamListener("word-count")
    public void process(KStream<String, WordCount> wordCount) {
        try {
            wordCount.foreach((key, value) ->
                    log.info("Received word count: " + value)
            );
        } catch (Throwable e) {
            log.error("Failed to process word count", e);
        }
    }

    @StreamListener("windowed-word-count")
    public void processWindowed(KStream<String, WordCount> wordCount) {
        try {
            wordCount.foreach((key, value) ->
                    log.info("Received windowed word count: " + value)
            );
        } catch (Throwable e) {
            log.error("Failed to process windowed word count", e);
        }
    }

    @ServiceActivator(inputChannel = "wordCount.wordCountGroup.errors")
    public void wordCountError(Message<?> message) {
        log.error("Windowed word count error: " + message);
    }

    @ServiceActivator(inputChannel = "windowedWordCount.windowedWordCountGroup.errors")
    public void windowedWordCountError(Message<?> message) {
        log.error("Windowed word count error: " + message);
    }
}
