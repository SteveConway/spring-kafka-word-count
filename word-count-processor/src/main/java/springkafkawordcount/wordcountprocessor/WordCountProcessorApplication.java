package springkafkawordcount.wordcountprocessor;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import springkafkawordcount.wordcountprocessor.spring.WordCountProcessorBinding;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@EnableBinding({WordCountProcessorBinding.class})
public class WordCountProcessorApplication {

    public static void main(String[] args) {
        run(WordCountProcessorApplication.class, args);
    }
}
