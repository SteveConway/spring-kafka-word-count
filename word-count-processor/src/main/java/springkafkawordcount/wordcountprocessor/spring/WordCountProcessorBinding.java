package springkafkawordcount.wordcountprocessor.spring;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import springkafkawordcount.common.model.event.TextEvent;
import springkafkawordcount.common.model.event.WordCountEvent;

public interface WordCountProcessorBinding {

    @Input("text-input")
    KStream<?, TextEvent> textInput();

    @Input("text-input-for-windowed")
    KStream<?, TextEvent> textInputForWindowed();

    @Output("word-count")
    KStream<?, WordCountEvent> wordCount();

    @Output("windowed-word-count")
    KStream<?, WordCountEvent> windowedWordCount();
}
